  $(document).ready(function(){
    $("#editItem").on("click", function(e) {
        e.preventDefault();
        var button = $(this);
        var ID = $(this).data('id');
        data = {};
        data.id = ID;
        $('#eqlComplete'+ID).show();
        $('#eqlTxtName'+ID).show();
        $(this).prev().show();
        $('#eqlName'+ID).hide();
        $(this).hide();
        $(this).next().hide();
    });
    $("#saveItem").on("click", function(e) {
        e.preventDefault();
        var button = $(this);
        var ID = $(this).data('id');
        data = {};
        data.id = ID;
        data.complete = $('#eqlComplete'+ID).prop("checked") ? 1 : 0;
        data.name = $('#eqlTxtName'+ID).value();
        $('#eqlComplete'+ID).hide();
        $('#eqlTxtName'+ID).hide();
        $(this).prev().hide();
        $('#eqlName'+ID).show();
        $(this).show();
        $(this).next().show();
        $.ajax({
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            url: '/updateEQL',
            success: function(data) {
                window.location.assign("/eql");
            }
        });
    }); 
  });          
