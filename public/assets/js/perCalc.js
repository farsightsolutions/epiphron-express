  $(document).ready(function(){
    $(".percToolTip").popover({
        content: function () {
            return '<div class="Table"><div class="Heading"><div class="Cell"><p>95%</p></div><div class="Cell"><p>90%</p></div><div class="Cell"><p>85%</p></div><div class="Cell"><p>80%</p></div><div class="Cell"><p>75%</p></div></div><div class="Row"><div class="Cell"><p>' + $(this).text()*.95 + '</p></div><div class="Cell"><p>' + $(this).text()*.9+'</p></div><div class="Cell"><p>' + $(this).text()*.85+'</p></div><div class="Cell"><p>' + $(this).text()*.8+'</p></div><div class="Cell"><p>' + $(this).text()*.75+'</p></div></div></div>'
        },
        html: true,
        placement: 'right',
        title: 'Weight Percentages',
        trigger: 'hover focus'
    }); 
  });