$(document).ready(function() {
  // Getting references to our form and input
  let signUpForm = $("form.signup");
  let emailInput = $("input#email-input");
  let passwordInput = $("input#password-input");
  let first_name = $("#first_name");
  let last_name = $("#last_name");
  // When the signup button is clicked, we validate the email and password are not blank
  signUpForm.on("submit", function(event) {
    event.preventDefault();
    var userData = {
      email: emailInput.val().trim(),
      password: passwordInput.val().trim(),
      first_name: first_name.val().trim(),
      last_name: last_name.val().trim()
    };

    if(!userData.first_name) {
      console.log(userData.first_name);
    }
    if (!userData.email || !userData.password || !userData.first_name || !userData.last_name) {
      return;
    }
    // If we have an email and password, run the signUpUser function
    signUpUser(userData.email, userData.password, userData.first_name, userData.last_name);
    emailInput.val("");
    passwordInput.val("");
    first_name.val("");
    last_name.val("");
  });

  // Does a post to the signup route. If succesful, we are redirected to the members page
  // Otherwise we log any errors
  function signUpUser(email, password, first_name, last_name) {
    $.post("/api/signup", {
      email: email,
      password: password,
      first_name: first_name,
      last_name: last_name
    }).then(function(data) {
      window.location.replace(data);
      // If there's an error, handle it by throwing up a boostrap alert
    }).catch(handleLoginErr);
  }

  function handleLoginErr(err) {
    $("#alert .msg").text(err.responseJSON);
    $("#alert").fadeIn(500);
  }
});
