import Strength from '../models/strengths';
const isAuthenticated = require("../config/middleware/isAuthenticated");
const moment = require('moment');
/* GET users listing. */

module.exports = function(app) {
	app.get('/strength', isAuthenticated, function(req, res, next) {
		let theStrength = new Strength({user_id:req.user.id});
		theStrength.renderStrengthView(req,res);
	});
	app.post('/addStrength', isAuthenticated, function(req, res, next) {
		let theStrength = new Strength({user_id:req.body.ddlMemberName, 
										rep_id: req.body.ddlReps, 
										movement_id: req.body.ddlMovement, 
										str_amount:req.body.txtWeight, 
										date: moment(new Date()).format("YYYY-MM-DD")
									});
		let insertPromise = theStrength.insertStrength();
		insertPromise.then(function(result) {
		  	if(result) {
		  		theStrength.renderStrengthView(req,res);
		  	}
		});
	});
}