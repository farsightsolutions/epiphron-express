import Movement from '../models/movements';
const isAuthenticated = require("../config/middleware/isAuthenticated");
/* GET users listing. */

module.exports = function(app) {
	app.get('/movements', isAuthenticated, function(req, res, next) {
		let theMovements = new Movement({id:null});
		theMovements.renderMovementsView(req,res);
	});
	app.post('/addMovement', function(req, res, next) {
	  let theMovement = new Movement({name:req.body.txtMovement});
	  let insertPromise = theMovement.insertMovement();
	  insertPromise.then(function(result) {
	  	if(result) {
	  		theMovement.renderMovementsView(req,res);
	  	}
	  });
	});
}