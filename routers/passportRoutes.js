// Requiring our models and passport as we've configured it
const auth = require("../config/passport");
const path = require("path");
const isAuthenticated = require("../config/middleware/isAuthenticated");
import User from '../models/user';
//
module.exports = function(app) {
  // Using the passport.authenticate middleware with our local strategy.
  // If the user has valid login credentials, send them to the members page.
  // Otherwise the user will be sent an error
  app.post("/api/login", function(req, res, next) {
    auth.authenticate("local", function(err, user, info) {
      if (err) { return next(err); }
      if (!user) { return res.redirect('/login'); }
      req.login(user, function(err) {
        if (err) { return next(err); }
        else {return res.json("/");}
      });
    })(req, res, next);
  });
  app.post("/api/signup", function(req, res) {
    let newUser = new User({email:req.body.email, first_name:req.body.first_name, last_name:req.body.last_name, password:req.body.password});
    let newUserPromise = newUser.registerUser();
    newUserPromise.then(function(result) {
        if(result) {return res.json("/login")}
    });
  });
  // Route for logging user out
  app.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/");
  });
  app.get("/login", function(req, res) {
    // If the user already has an account send them to the members page
    if (req.user) {
      res.redirect("/");
    } else {
      res.locals.title = 'Login';
      res.render('Auth/auth-login', { 'message': req.flash('message'), 'error': req.flash('error') });      
    }
  });
  app.get('/register', function (req, res) {
    if (req.user) { res.redirect('Dashboard/index'); }
    else {
      res.render('Auth/auth-register', { 'message': req.flash('message'), 'error': req.flash('error') });
    }
  });
  app.get('/forgot-password', function (req, res) {
    res.render('Auth/auth-forgot-password', { 'message': req.flash('message'), 'error': req.flash('error') });
  });
};
