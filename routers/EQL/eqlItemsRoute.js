import EQLItems from '@models/EQL/eqlItems';
const isAuthenticated = require("@config/middleware/isAuthenticated");
const moment = require('moment');
/* GET users listing. */

module.exports = function(app) {
	app.get('/eql', isAuthenticated, function(req, res, next) {
		let eqlItem = new EQLItems({user_id:req.user.id});
		eqlItem.renderEQLListView(req,res);
	});
	app.post('/addEQL', isAuthenticated, function(req, res, next) {
		let eqlItem = new EQLItems({user_id:req.user.id, 
									item_name: req.body.item_name,
									category_id: req.body.ddlCategory,
									completed: 0,
									usedxp: 0
								});
		let insertPromise = eqlItem.insertListItem();
		insertPromise.then(function(result) {
		  	if(result) {
		  		eqlItem.renderEQLListView(req,res);
		  	}
		});
	});
}
