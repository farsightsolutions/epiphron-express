import EQLCategories from '@models/EQL/eqlCategories';
const isAuthenticated = require("@config/middleware/isAuthenticated");
const moment = require('moment');
/* GET users listing. */

module.exports = function(app) {
	app.get('/eqlCategories', isAuthenticated, function(req, res, next) {
		let eqlCategory = new EQLCategories({company_id:1});
		eqlCategory.renderCategoryView(req,res);
	});
	app.post('/addEQLCategory', isAuthenticated, function(req, res, next) {
		let eqlCategory = new EQLCategories({company_id:1, 
										category_name: req.body.category_name
									});
		let insertPromise = eqlCategory.insertCategoryItem();
		insertPromise.then(function(result) {
		  	if(result) {
		  		eqlCategory.renderCategoryView(req,res);
		  	}
		});
	});
}