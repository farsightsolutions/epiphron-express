import Reps from '../models/reps';
const isAuthenticated = require("../config/middleware/isAuthenticated");
/* GET users listing. */

module.exports = function(app) {
	app.get('/reps', isAuthenticated, function(req, res, next) {
		let theReps = new Reps({id:null});
		theReps.renderRepsView(req,res);
	});
	app.post('/addReps', isAuthenticated, function(req, res, next) {
	  let theReps = new Reps({amount:req.body.txtReps});
	  let insertPromise = theReps.insertReps();
	  insertPromise.then(function(result) {
	  	if(result) {
	  		theReps.renderRepsView(req,res);
	  	}
	  });
	});
}