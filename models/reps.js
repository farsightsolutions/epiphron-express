const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
import Generics from './generics';

export default class Reps extends Generics {
  constructor({id, amount}) {
    super(id);
    this.rep_amount = amount;
  }

  createTable() {
    let createQuery = "CREATE TABLE IF NOT EXISTS `reps` (`rep_id` int NOT NULL AUTO_INCREMENT,`rep_amount` varchar(45) DEFAULT NULL, PRIMARY KEY (`rep_id`),";
    createQuery += " UNIQUE KEY `rep_amount_UNIQUE` (`rep_amount`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    dbconfig.getConnection(function(err,connection) {
      connection.query(createQuery,[],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Create Failed - Create Table: ' + err});
        }
      });
    });
  }

  insertReps() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let insertQuery = "INSERT INTO reps (rep_amount) VALUES (?)";
      dbconfig.getConnection(function(err,connection) {
        connection.query(insertQuery,[self.rep_amount],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Add Failed - Add Reps: ' + err}));
          } else {
            fulfill(true);
          }
        });
      });
    });
  }

  updateReps() {
    let updateQuery = "UPDATE reps SET rep_amount = ? WHERE rep_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[reps.rep_amount, reps.rep_id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - Update Reps: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  deleteReps() {
    let deleteQuery = "DELETE FROM reps WHERE rep_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(deleteQuery,[Asset_Name,Status,Asset_Serial],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Delete Failed - Delete Reps: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  renderRepsView(req, res) {
    let allReps = super.list("SELECT * FROM reps");
    allReps.then(function(reps) {
      res.locals.title = 'Reps';
      res.render('reps.ejs', {
        reps: reps
      });
    });
  }
}