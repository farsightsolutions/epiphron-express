const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;

export default class Generic{
  constructor(id){
      this.id = id;
  }
  list(selectQuery){
    return new Promise(function(fulfill, reject) {
      dbconfig.getConnection(function(err,connection) {
        connection.query(selectQuery,[],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Select Failed - Select Generic: ' + err}));
          } else{
            fulfill(result);
          }
        });
      });
    });  
  }
  listParams(selectQuery, params){
    return new Promise(function(fulfill, reject) {
      dbconfig.getConnection(function(err,connection) {
        connection.query(selectQuery,params,function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Select Failed - Select Params Generic: ' + err}));
          } else{
            fulfill(result);
          }
        });
      });
    });
  }
}