const dbconfig = require('@config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
import Generics from '@models/generics';

export default class EQL extends Generics {
  constructor({id, user_id, item_name, category_id, completed, usedxp}) {
    super(id);
    this.user_id = user_id;
    this.item_name = item_name;
    this.category_id = category_id;
    this.completed = completed;
    this.usedxp = usedxp;
  }

  createEQLItemTable() {
    let createQuery = "CREATE TABLE IF NOT EXISTS `eql_items` (`item_id` int NOT NULL AUTO_INCREMENT, `user_id` int DEFAULT NULL, "
    createQuery+= "`item_name` varchar(145) DEFAULT NULL, `category_id` int DEFAULT NULL, `completed` tinyint DEFAULT 0, "
    createQuery += "`usedxp` tinyint DEFAULT 0, PRIMARY KEY (`item_id`))";
    createQuery += " ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    dbconfig.getConnection(function(err,connection) {
      connection.query(createQuery,[],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Create Failed - Create Table: ' + err});
        }
      });
    });
  }

  insertListItem() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let insertQuery = "INSERT INTO eql_items (user_id, item_name, category_id) VALUES (?,?,?)";
      dbconfig.getConnection(function(err,connection) {
        connection.query(insertQuery,[self.user_id, self.item_name, self.category_id],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Add Failed - Add EQL Item: ' + err}));
          } else {
            fulfill(true);
          }
        });
      });
    });
  }

  updateListItem() {
    let self = this;
    let updateQuery = "UPDATE eql_items SET item_name = ?, category_id = ? WHERE item_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[self.item_name, self.category_id, self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - Update EQL Item: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  completeListItem() {
    let self = this;
    let updateQuery = "Update eql_items SET completed = 1 WHERE item_ID = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - MNArk EQL Item Completed: ' + err});
        } else{
          //TODO
        }
      });
    });
  }
  
  deleteListItem() {
    let self = this;
    let deleteQuery = "DELETE FROM eql_items WHERE item_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(deleteQuery,[self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Delete Failed - Delete EQL Item: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  renderEQLListView(req, res) {
    let self = this;
    let selectQuery = "SELECT * FROM eql_items INNER JOIN eql_categories on eql_items.category_id = eql_categories.category_id ";
    selectQuery += "where user_id = ? ORDER BY eql_items.category_id";
    let companyCategories = super.listParams("SELECT * from eql_categories where company_id = ?", [1]);
    companyCategories.then(function(categories) {
      dbconfig.getConnection(function(err,connection) {
        connection.query(selectQuery,[self.user_id],function(err,result) {
          connection.release();
          if(err) {
            console.log({status: 1, message: 'Select Failed - List EQL Item: ' + err});
          } else{
            res.locals.title = 'Epic Quest List';
            res.render('EQL/eql.ejs', {
              eqlcategories: categories,
              eqlitems: result
            });
          }
        });
      });
    });
  }

}