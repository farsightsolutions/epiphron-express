const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
import Generics from './generics';

export default class EQL extends Generics {
  constructor({id, user_id, level}) {
    super(id);
    this.user_id = user_id;
    this.level = level
  }

  createEQLUserLevelTable() {
    let createQuery = "CREATE TABLE IF NOT EXISTS `eql_user_level` (`user_level_id` int NOT NULL AUTO_INCREMENT, `user_id` int DEFAULT NULL, "
    createQuery+= "`level` int DEFAULT NULL, PRIMARY KEY (`user_level_id`))";
    createQuery += " ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    dbconfig.getConnection(function(err,connection) {
      connection.query(createQuery,[],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Create Failed - Create Table: ' + err});
        }
      });
    });
  }

  insertUserLevel() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let insertQuery = "INSERT INTO eql_user_level (user_id) VALUES (?, 1)";
      dbconfig.getConnection(function(err,connection) {
        connection.query(insertQuery,[self.user_id],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Add Failed - Add User Level: ' + err}));
          } else {
            fulfill(true);
          }
        });
      });
    });
  }

  updateUserLevel() {
    let self = this;
    let updateQuery = "UPDATE eql_user_level SET level = ? WHERE user_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[self.level, self.user_id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - Update User Level: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  deleteUserLevel() {
    let self = this;
    let deleteQuery = "DELETE FROM eql_user_level WHERE user_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(deleteQuery,[self.user_id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Delete Failed - Delete User Level: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  renderMovementsView(req, res) {
    let allMovements = super.list("SELECT * FROM movements");
    allMovements.then(function(movements) {
      res.locals = { title: 'Movements' };
      res.render('movements.ejs', {
        movements: movements,
        login: true
      });
    });
  }

}