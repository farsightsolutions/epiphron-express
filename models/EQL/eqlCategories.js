const dbconfig = require('@config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
import Generics from '@models/generics';

export default class EQLCategories extends Generics {
  constructor({id, company_id, category_name}) {
    super(id);
    this.company_id = company_id;
    this.category_name = category_name;
  }

  createEQLCategoriesTable() {
    let createQuery = "CREATE TABLE IF NOT EXISTS `eql_categories` (`category_id` int NOT NULL AUTO_INCREMENT, `company_id` int DEFAULT NULL, "
    createQuery+= "`category_name` varchar(145) DEFAULT NULL, PRIMARY KEY (`category_id`))";
    createQuery += " ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    dbconfig.getConnection(function(err,connection) {
      connection.query(createQuery,[],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Create Failed - Create Table: ' + err});
        }
      });
    });
  }

  insertCategoryItem() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let insertQuery = "INSERT INTO eql_categories (company_id, category_name) VALUES (?,?)";
      dbconfig.getConnection(function(err,connection) {
        connection.query(insertQuery,[self.company_id, self.category_name],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Add Failed - Add EQL Category: ' + err}));
          } else {
            fulfill(true);
          }
        });
      });
    });
  }

  updateCategoryItem() {
    let self = this;
    let updateQuery = "UPDATE eql_categories SET category_name = ? WHERE category_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[self.category_name, self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - Update EQL Categories: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  deleteCategoryItem() {
    let self = this;
    let deleteQuery = "DELETE FROM eql_categories WHERE category_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(deleteQuery,[self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Delete Failed - Delete EQL Categories: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  renderCategoryView(req, res) {
    let self = this;
    let selectQuery = "SELECT * FROM eql_categories where company_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(selectQuery,[self.company_id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Select Failed - List EQL Item: ' + err});
        } else{
          res.locals.title = 'Epic Quest List - Categories';
          res.render("EQL/eql_categories.ejs", {
            eqlcategories: result,
          });
        }
      });
    });
  }
}