import Reps from '../models/reps';
import Movements from '../models/movements';
import Strength from '../models/strengths';

function DatabaseBuilds() {
	this.buildTable = function(table) {
		const tableNames = {
			reps:"Reps",
			movements:"Movements"
		}
		console.log(tableNames[table]);
		let tableBuild = new tableNames[table]({id:null});
		tableBuild.createTable();
	}
	this.buildRepsTable = function() {
		const newReps = new Reps({id:null});
		newReps.createTable();
	}
	this.buildMovementsTable = function() {
		const newMovement = new Movements({id:null});
		newMovement.createTable();
	}
	this.buildStrengthsTable = function() {
		const newStrength = new Strength({id:null});
		newStrength.createTable();
	}
}

module.exports = new DatabaseBuilds();