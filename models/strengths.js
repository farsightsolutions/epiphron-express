const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
const moment = require('moment');
import Generics from './generics';

export default class Strengths extends Generics{
  constructor({id, user_id, rep_id, movement_id, str_amount, date}) {
    super(id);
    this.user_id = user_id;
    this.rep_id = rep_id;
    this.movement_id = movement_id;
    this.str_amount = str_amount;
    this.date = moment(new Date()).format("YYYY-MM-DD");
  }

  createTable() {
    let createQuery = "CREATE TABLE IF NOT EXISTS `strengths` (`strength_id` int NOT NULL AUTO_INCREMENT,`user_id` int(11) DEFAULT NULL, `rep_id` int(11) DEFAULT NULL,";
    createQuery += " `movement_id` int(11) DEFAULT NULL, `str_amount` int(11) DEFAULT NULL, `date` DATE DEFAULT NULL,";
    createQuery += " PRIMARY KEY (`strength_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    dbconfig.getConnection(function(err,connection) {
      connection.query(createQuery,[],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Create Failed - Create Table: ' + err});
        }
      });
    });
  }

  insertStrength() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let insertQuery = "INSERT INTO strengths (user_id, rep_id, movement_id, str_amount, date) VALUES (?,?,?,?,?)";
      dbconfig.getConnection(function(err,connection) {
        connection.query(insertQuery,[self.user_id, self.rep_id, self.movement_id, self.str_amount, self.date],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Add Failed - Add Strength: ' + err}));
          } else {
            fulfill(true);
          }
        });
      });
    });
  }

  listStrength() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let selectQuery = "SELECT str_amount, s.rep_id, movement_name, s.date, m.movement_id FROM strengths AS s INNER JOIN movements As m";
      selectQuery +=" ON s.movement_id = m.movement_id WHERE strength_id IN ( SELECT swid FROM ( SELECT movement_id, rep_id, MAX(date) AS wd, MAX(strength_id) AS swid FROM strengths";
      selectQuery +=" WHERE user_id = ? GROUP BY rep_id, movement_id) As T1) ORDER BY movement_id, rep_id ASC";
      dbconfig.getConnection(function(err,connection) {
        connection.query(selectQuery,[self.user_id],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Select Failed - Select Strengths: ' + err}));
          } else{
            fulfill(result);
          }
        });
      });
    });    
  }

  updateStrength() {
    let self = this;
    let updateQuery = "UPDATE strengths SET movement_name = ? WHERE movement_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[self.movement_name, self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - Update Strength: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  deleteStrength() {
    let self = this;
    let deleteQuery = "DELETE FROM strengths WHERE strength_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(deleteQuery,[self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Delete Failed - Delete Reps: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  renderStrengthView(req, res) {
    let allMovements = super.list("SELECT * FROM movements");
    let allReps = super.list("SELECT * FROM reps");
    let allUsers = super.list("SELECT id, first_name, last_name FROM users")
    let userStrength = this.listStrength();
    Promise.all([allMovements, allReps, userStrength, allUsers]).then(function(values) {
      res.locals.title = 'Strength';
      res.render('strength.ejs', {
        strengths: values[2],
        reps: values[1],
        movements: values[0],
        users: values[3],
        moment: moment,
        login: true
      });
    });
  }

}