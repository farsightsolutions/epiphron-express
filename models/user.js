// Requiring bcrypt for password hashing. Using the bcryptjs version as 
//the regular bcrypt module sometimes causes errors on Windows machines
const bcrypt = require("bcryptjs");
const saltRounds = 10;
const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
//
// Creating our User model
//Set it as export because we will need it required on the server

export default class User {
  constructor({email, first_name, last_name, password, salt, id, hash}) {
    this.id = id;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.salt = salt;
    this.password = password;
    this.hash = hash;
  }

  registerUser() {
    const self = this; 
    //generate salt
    return new Promise(function(fulfill, reject) {
      bcrypt.genSalt(saltRounds, function(err, salter) {
        self.salt = salter;
        bcrypt.hash(self.password, self.salt, function(err, hashed) {
          self.hash = hashed;
          let insertQuery = "INSERT INTO users (email, hash, first_name, last_name, salt) VALUES (?,?,?,?,?)";
          dbconfig.getConnection(function(err,connection) {
            connection.query(insertQuery,[self.email, self.hash, self.first_name, self.last_name, self.salt],function(err,result) {
              connection.release();
              if(err) {
                reject(console.log({status: 1, message: 'Insert Failed - Insert User: ' + err}));
              } else { fulfill(true); }
            });
          });
        });
      });
    });
  }

  findUser() {
    const self = this;
    return new Promise(function(fulfill, reject) {
      let selectQuery = "SELECT * from weightlifting.users where email = ?";
      dbconfig.getConnection(function(err,connection) {
        connection.query(selectQuery,[self.email],function(err,result) {
          connection.release();
          if(err) {
            console.log({status: 1, message: 'Insert Failed - Insert User: ' + err});
          }
          if(result.length == 0) {
            fulfill(false);
          } else {
            bcrypt.hash(self.password, result[0].salt, function(err, hashed) {
              if(result[0].hash == hashed) {
                fulfill(result[0]);
              } else {
                fulfill(false);
              }
            });
          }
        });
      });
    });
  }
}