const dbconfig = require('../config/db.'+ (process.env.NODE_ENV.trim() || "development") +'.js').pool;
const Promise = require('promise');
import Generics from './generics';

export default class Movements extends Generics {
  constructor({id, name}) {
    super(id);
    this.movement_name = name;
  }

  createTable() {
    let createQuery = "CREATE TABLE IF NOT EXISTS `movements` (`movement_id` int NOT NULL AUTO_INCREMENT,`movement_name` varchar(145) DEFAULT NULL, PRIMARY KEY (`movement_id`),";
    createQuery += " UNIQUE KEY `movement_name_UNIQUE` (`movement_name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    dbconfig.getConnection(function(err,connection) {
      connection.query(createQuery,[],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Create Failed - Create Table: ' + err});
        }
      });
    });
  }

  insertMovement() {
    let self = this;
    return new Promise(function(fulfill, reject) {
      let insertQuery = "INSERT INTO movements (movement_name) VALUES (?)";
      dbconfig.getConnection(function(err,connection) {
        connection.query(insertQuery,[self.movement_name],function(err,result) {
          connection.release();
          if(err) {
            reject(console.log({status: 1, message: 'Add Failed - Add Movement Name: ' + err}));
          } else {
            fulfill(true);
          }
        });
      });
    });
  }

  updateMovements() {
    let self = this;
    let updateQuery = "UPDATE movements SET movement_name = ? WHERE movement_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(updateQuery,[self.movement_name, self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Update Failed - Update Movements: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  deleteMovement() {
    let self = this;
    let deleteQuery = "DELETE FROM movements WHERE movement_id = ?";
    dbconfig.getConnection(function(err,connection) {
      connection.query(deleteQuery,[self.id],function(err,result) {
        connection.release();
        if(err) {
          console.log({status: 1, message: 'Delete Failed - Delete Reps: ' + err});
        } else{
          //TODO
        }
      });
    });    
  }

  renderMovementsView(req, res) {
    let allMovements = super.list("SELECT * FROM movements");
    allMovements.then(function(movements) {
      res.locals = { title: 'Movements' };
      res.render('movements.ejs', {
        movements: movements,
        login: true
      });
    });
  }

}