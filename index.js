require("@babel/register")({
  presets: ["@babel/preset-env"]
});
require('module-alias/register');
const express = require('express');
const path = require('path');
const validator = require('express-validator');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
const createError = require('http-errors');
const logger = require('morgan');

const config = require("./config/configFile.js").get(process.env.NODE_ENV.trim());

// import controller
//const AuthController = require('./controllers/AuthController');

// import Router file
//const pageRouter = require('./routers/route');
const passport = require("./config/passport");
const databaseBuilds = require("./models/databaseBuilds");

const app = express();
const port = config.port;

const bodyParser = require('body-parser');
const flash = require('connect-flash');
const i18n = require("i18n-express");

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(cookieSession({
  name: 'session',
  key: config.session.key,
  secret: config.session.secret,
  cookie: {maxAge: 1000*60*60}
})); // session secret

app.use(flash());
app.use(i18n({
  translationsPath: path.join(__dirname, 'i18n'), // <--- use here. Specify translations files path.
  siteLangs: ["sp", "en", "gr", "rs", "it"],
  textsVarName: 'translation'
}));

app.use('/public', express.static('public'));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.get('/layouts/', function (req, res) {
  res.render('view');
});

// apply controller
//AuthController(app);
app.use(passport.initialize());
app.use(passport.session());

require('./routers/passportRoutes.js')(app);
//For set layouts of html view
var expressLayouts = require('express-ejs-layouts');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);

// Define All Route 
//pageRouter(app);
require('./routers/index.js')(app); // load our routes and pass in our app
require('./routers/repsRoutes.js')(app);
require('./routers/movementsRoutes.js')(app);
require('./routers/strengthRoutes.js')(app);
require('./routers/EQL/eqlCategoryRoute.js')(app);
require('./routers/EQL/eqlItemsRoute.js')(app);

app.get("/", function(req, res) {
  if (req.user) {
    try{
      res.locals.title = 'Woz Technologies';
      res.locals.user = {first_name: req.user.first_name, last_name: req.user.last_name};
      res.render('index.ejs');
    } catch (err) {
      console.log(err);
    }
  } else {
    res.redirect('/login');
  }    
});

app.listen(port, ()=> {
  databaseBuilds.buildRepsTable();
  databaseBuilds.buildMovementsTable();
  databaseBuilds.buildStrengthsTable();
  console.log('listening on *:'+port);
});
