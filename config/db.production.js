const path = require('path');
const mysql = require('mysql');
const config = require(path.resolve( __dirname,'configFile.js')).get(process.env.NODE_ENV.trim());

let pool = mysql.createPool({
	'connectionLimit': config.database.connectionLimit,
    'host': config.database.host,
    'user': config.database.user,
    'password': config.database.password,
	'database': config.database.database
});

exports.pool = pool;