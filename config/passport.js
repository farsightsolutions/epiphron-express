//we import passport packages required for authentication
import User from '../models/user';
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
//
//We will need the models folder to check passport agains
//
// Telling passport we want to use a Local Strategy. In other words,
//we want login with a username/email and password
passport.use(new LocalStrategy(
  // Our user will sign in using an email, rather than a "username"
  {
    usernameField: "email",
  },
  function(email, password, done) {
    // When a user tries to sign in this code runs
    let userLog = new User({email:email, password:password});
    let userPromise = userLog.findUser();
    userPromise.then(function(user) {
      if(!user) {
        return done(null, false, {
          message: "Invalid email or password"
        });   
      } else {
        return done(null,user);
      } 
    }).catch(function(rejectExtraction) {
      console.log(rejectExtrtraction);
    });
  }
));
//
// In order to help keep authentication state across HTTP requests,
// Sequelize needs to serialize and deserialize the user
// Just consider this part boilerplate needed to make it all work
passport.serializeUser(function(user, done) {
  done(null, user);
});
//
passport.deserializeUser(function(user, done) {
  done(null, user);
});
//
// Exporting our configured passport
module.exports = passport;