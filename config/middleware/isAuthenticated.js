// This is middleware for restricting routes a user is not allowed to visit if not logged in
module.exports = function(req, res, next) {
  // If the user is logged in, continue with the request to the restricted route
  if (req.user) {
  	res.locals.user = {first_name: req.user.first_name, last_name: req.user.last_name};
    return next();
  }
  // If the user isn't' logged in, redirect them to the login page
  return res.redirect("/");
};