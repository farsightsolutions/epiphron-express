var config = {
  production: {
    port: 49155,
    session: {
      key: 'the.express.session.id',
      secret: 'correcthorsebatterystaple'
    },
    database: {
      connectionLimit: 100,
      host: 'Woz-s01',
      user: 'cwozniak',
      password: '122801Cv',
      database: 'weightlifting'
    }
  },
  development: {
    port: 8099,
    session: {
      key: 'the.express.session.id',
      secret: 'correcthorsebatterystaple'
    },
    database: {
      connectionLimit: 100,
      host: 'Woz-s01',
      user: 'cwozniak',
      password: '122801Cv',
      database: 'weightlifting'
    }
  }
}

exports.get = function get(env) {
  return config[env] || config.development;
}